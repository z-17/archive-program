#include "photoopendialog.h"
#include "ui_photoopendialog.h"
#include <QDir>
#include <QFileInfo>
#include <QFileInfoList>
#include <QIcon>
#include <QPixmap>
#include <QSettings>
#include <QFileDialog>
#include <QEventLoop>
#include <QDebug>


PhotoOpenDialog::PhotoOpenDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PhotoOpenDialog)
    ,m_pSettings(0)
    ,m_bProcessingFolder(0)
    ,m_bStopProcessingFolder(0)
{
    ui->setupUi(this);


    restoreSettings();
    m_nIconSize = ui->cbIconSize->currentText().toInt();
    setStatus(tr("Use Folder..., Open or select folder from pulldown list"));
}

PhotoOpenDialog::~PhotoOpenDialog()
{

    delete m_pSettings;

    delete ui;

}

// Этот месседж лайн(нижняя строчка прогарммы)
void PhotoOpenDialog::setStatus( QString sts )
{
    ui->lbStatus->setText( sts );
}

// Проверка на уже сжатое изображение
bool PhotoOpenDialog::isResizedImage( QString path )
{
    QString foo;

    // filenamexnnnn.jpg
    QRegExp re(QString("x{1,1}[0-9]{1,4}\\.{1,1}"), Qt::CaseInsensitive );

    return path.contains( re );
}

// Установка названия окна
void PhotoOpenDialog::setTitle( QString title )
{
    setWindowTitle( title );
}

// Тип выбора клиентом
void PhotoOpenDialog::setSelectionMode( QAbstractItemView::SelectionMode mode)
{
    ui->twFolder->setSelectionMode( mode );
}

//
// Возвращает выбранные пички
// которые включены в список, но ен помещаются на превью
//
// В колонке один - пути и над 2 колонкой - название пикчи
QStringList PhotoOpenDialog::selectedFiles()
{
    QStringList files;
    QList<QTreeWidgetItem*> items;

    items = ui->twFolder->selectedItems();

    for ( int i=0; i<items.count(); i++ )
    {
        QString path = items[i]->text(2); // get file path from hidden column
        if ( path.count() )
        {
            files.append( path );
        }
    }
    return files;
}

//
// Save the list of folders we have seen.
// so that each time we open the dialog, the list of recent folders is there.
//
// Create a settings object the first time we are called
// It's an ini file in the user folder.
// Avoid using the registry on Windows by using an ini-format.
//
void PhotoOpenDialog::createSettings()
{
    if ( !m_pSettings )
    {
        m_pSettings = new QSettings( QSettings::IniFormat, QSettings::UserScope,
                                         QString("WindyWeather"), QString("PhotoOpenDialog") );
    }
}

//
// save the most recent two dozen paths from the list
// that's enough for anybody and they might build up over time to very large numbers
//
void PhotoOpenDialog::saveSettings()
{
    createSettings();

    m_pSettings->beginGroup("PhotoFolders");

    int ncnt = ui->cbFolderPath->count();
    if ( ncnt > 24 ) ncnt = 24; // that's long enough

    m_pSettings->setValue( "FolderCount", ncnt );

    for ( int i=0; i < ncnt; i++ )
    {
        // we will fix the paths on load, so we don't have to fix now
        m_pSettings->setValue( QString("Folder_%1").arg(i), ui->cbFolderPath->itemText(i) );
    }
    m_pSettings->endGroup();
}

//
// restore the paths from the settings file.
// we are going to sanitize them as we read them
// this will clean up anything from older versions.
// only interesting for me during development actually.
// Or if we change the sanitization after release.
//
void PhotoOpenDialog::restoreSettings()
{
    createSettings();

    m_pSettings->beginGroup("PhotoFolders");

    int ncnt = m_pSettings->value( "FolderCount", 0 ).toInt();

    for ( int i=0; i < ncnt; i++ )
    {
        QString folder;
        folder = m_pSettings->value( QString("Folder_%1").arg(i), "" ).toString();
        // if the folder has a length, and it's not that funny windows degenerate
        // case that we stored by mistake before we fixed the bug, then use it.
        if ( folder.count() && folder != QString("\\\\") )
        {
            // sanitize the path and then insert it into the list
            // in the order it came from the list.
            QString fixedFolder = fixFolderPath( folder );
            // remove any duplicates as we insert things
            // again, this is not normally going to be a problem
            if ( ui->cbFolderPath->findText(fixedFolder) == -1 )
            {
                ui->cbFolderPath->addItem( fixedFolder );
            }
        }
    }
    m_pSettings->endGroup();

    ui->cbFolderPath->setCurrentIndex(0); // point to top of list - most recent
}

//
// close event from the X on window or something else.
//
void PhotoOpenDialog::closeEvent ( QCloseEvent * event )
{
    // don't hold this up if we are processing folders.
    // Just close the dialog, but save settings and send
    // reject signal on the way out

    // save settings since the user may have visited interesting folders
    saveSettings();
    reject();
    // just say so we know we really came here
    qDebug("PhotoOpenDialog::closeEvent");
}

//
// cancel the dialog
// we will save the folders since the novice user will expect it.
//
void PhotoOpenDialog::on_btnCancel_clicked()
{
    if ( m_bProcessingFolder )
    {
        m_bStopProcessingFolder = true;
        setStatus(tr("Folder preview stopped"));
        return;
    }
    // save settings since the user may have visited interesting folders
    saveSettings();
    reject();
}

//
// Open.
// If we haven't opened a folder yet, then open the top of the folder path box.
//
// If we have no selected items, then Open makes no sense, so just kick a message
// and ignore it.
//
// If there are selected items, then accept and the client can call us back to get them.
//
void PhotoOpenDialog::on_btnOpen_clicked()
{
    if ( m_bProcessingFolder )
    {
        setStatus(tr("Use Cancel to stop"));
        return;
    }
    if ( ui->twFolder->topLevelItemCount() == 0 )
    {
        // if there are no items in the list, then take this to mean to display the folder from
        // the combo box.
        on_cbFolderPath_activated(ui->cbFolderPath->itemText( 0 ));
        return;
    }

    if ( ui->twFolder->selectedItems().count() == 0 )
    {
        // no photos selected so just say so and await user
        setStatus(tr("No Photos selected"));
        return;
    }
    saveSettings();
    accept();
}

//
// Pick a new folder that we haven't seen before.
// Of course the user can type into the combo box too.
// But most will use this.
//
void PhotoOpenDialog::on_btnBrowseFolder_clicked()
{
    if ( m_bProcessingFolder )
    {
        setStatus(tr("Use Cancel to stop"));
        return;
    }

    QString dir = QFileDialog::getExistingDirectory(this, tr("Photo Folder"),
                                                     ui->cbFolderPath->itemText(0),
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);

    if ( dir.count() != 0 ) // if cancel, then empty string
    {
        // fixup and insert the path in the list
        // then scan the list.
        on_cbFolderPath_activated( dir );
    }
}

//
// Change the preview size with the pulldown box.
//
void PhotoOpenDialog::on_cbIconSize_activated(QString iconSize )
{
    int nIconSize = iconSize.toInt();

    if ( nIconSize != m_nIconSize )
    {
        m_nIconSize = nIconSize;
        ui->twFolder->setIconSize( QSize( nIconSize, nIconSize ) );

        // don't need to rescan since we put the largest size icons in the
        // tree, if we change the size it will resize them for us.
        //on_dbFolderPath_activated( ui->dbFolderPath->currentText() );

    }
}

//
// Scan a folder
//
// Insert the folder in the combobox,
// scan for folders first so they are at the top of the list.
//
// Then scan for photos.
//
void PhotoOpenDialog::on_cbFolderPath_activated(QString infolder)
{

    // don't allow starting another folder processing if we are busy
    if ( m_bProcessingFolder )
    {
        setStatus(tr("Use Cancel to stop"));
        return;
    }
    m_bProcessingFolder = true;         // we are busy now with this folder
    m_bStopProcessingFolder = false;    // and we have not been stopped
    // don't both allowing a stop while we are processing folders, since
    // they don't take very long.


    // fix and insert the path in the folder list
    QString folder = fixAndInsertPath( infolder );

    setStatus( tr("Open Folder: %1").arg(folder) );

    ui->progressBar->setValue(0);
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(0); // Set busy indicator while we do the folders

    m_nIconSize = ui->cbIconSize->currentText().toInt();
    ui->twFolder->setIconSize( QSize( m_nIconSize, m_nIconSize ) );

    QDir dir(folder);

    QFileInfoList files = dir.entryInfoList(QDir::AllDirs | QDir::NoDotAndDotDot );

    ui->twFolder->clear();
    int ndirs = files.count();

    for ( int i=0; i< ndirs; i++ )
    {
        QTreeWidgetItem* pitem = new QTreeWidgetItem();
        QFileInfo ifo = files[i];

        QIcon iopen (":/images/foldericonx96.png");
        //QIcon iclosed(":/images/Folder_Icon_Closed.png");
        pitem->setIcon(0, iopen);

        pitem->setText(0, ifo.fileName() ); // just the filename to the visible column
        pitem->setText(1, ifo.filePath() ); // complete path, invisible
        ui->twFolder->insertTopLevelItem( i, pitem );
    }


    // now scan for files using a filter.
    QStringList filters;
    filters << "*.jpg" << "*.png" << "*.gif" << "*.svg" << "*.tif";
    dir.setNameFilters(filters);

    files = dir.entryInfoList( QDir::Files );
    int nfiles = files.count();
    if ( nfiles == 0 )
    {
        ui->progressBar->setMaximum( 10 ); // stop busy indicator for progress bar
    }
    else
    {   // set progress bar correctly
        ui->progressBar->setMaximum(nfiles);
    }

    // we are going to process events after every pixmap
    // so we can cancel out of this
    QEventLoop  eventLoop;

    // it takes long enough to scan the preview images that we should use
    // a progress bar to indicate how long it's taking for the novice user.
    for ( int i=0; i< nfiles; i++ )
    {
        QFileInfo ifo = files[i];

        QString fname = ifo.fileName();
        if ( !isResizedImage( fname ) )
        {
            QTreeWidgetItem* pitem = new QTreeWidgetItem();

            pitem->setText(0, ifo.fileName() ); // Just the filename in column 0
            pitem->setText(2, ifo.filePath() ); // complete path, invisible
            QIcon   thumbnail;
            QPixmap pixmap;

            // load the large preview and then the treewidget will
            // let us change to smaller icons automatically.
            if ( pixmap.load( ifo.filePath() ) )
            {
                pixmap = pixmap.scaled(256,256, Qt::KeepAspectRatio );
                thumbnail.addPixmap( pixmap );
                pitem->setIcon(0, thumbnail );
            }
            else
            {
                qDebug() << QString("PhotoOpenDialog - Error loading pixmap: ").toLatin1() << ifo.filePath().toLatin1();
            }
            // insert the photos after the folders
            ui->twFolder->insertTopLevelItem( ui->twFolder->topLevelItemCount(), pitem );
        }
        // Jump the progress bar so it ends in the right place.
        ui->progressBar->setValue( i+1 );

        // process events so cancel can bust us out of here
        eventLoop.processEvents(); // ignore the return and check our cancel flag
        // set by cancel button while we processed events.
        // all other events are tied off for this window

        if ( m_bStopProcessingFolder )
        {
            // so to stop we clear the window since it's not complete and then exit this loop
            ui->twFolder->clear();
            setStatus(tr("Preview of folder cancelled"));
            break; // exit the for loop
        }
    }

    m_bStopProcessingFolder = false;
    m_bProcessingFolder = false;

    // no matter what came before, indicate we are done
    ui->progressBar->setMinimum(0);
    ui->progressBar->setMaximum(100);
    ui->progressBar->setValue(100);
}

//
// Dclick on a folder item in the list.
// Add to the folderlist cbox and thenopen it up
//
void PhotoOpenDialog::on_twFolder_itemActivated(QTreeWidgetItem* item, int column)
{
    // activate - double click - on a folder opens it and inserts in combo box
    if ( item && item->text(1).count() )
    {
        setStatus( tr("Open Folder: %1").arg(item->text(1)) );
        QString folder = item->text(1); // hidden path for a folder
        on_cbFolderPath_activated( folder );
    }
}

//
// fix the nonsense with the separators
// and make the fix portable to linux / max where there is no confusion
// about the handedness of slashes.
// Note that Qt on windows sometimes returns slashes in paths,
// and we don't want to show slashes to our novice users since it might confuse.
// Also it allows "duplicate" paths to appear in folder list, which we don't want
//
QString PhotoOpenDialog::fixFolderPath( QString path )
{
    // replace backslashes with slashes
    QChar sep = QDir::separator();  // let's make this portable.
    QString fixpath;

    // check if we are on windows, if so, remove slashes and replace with back slashes
    // sometimes, Qt shows paths with slashes, but this might confuse our novice users, so always show
    // backslashes
    if ( sep == '\\' )
    {
        fixpath = path.replace( QChar('/'), QString("\\") );
    }
    else
    {
        // if linux, or mac, we don't need to turn everything into backslashes
        fixpath = path;
    }

    // now make sure that the path ends in a separator, just for consistency
    int idx = fixpath.lastIndexOf( sep );
    if ( idx != fixpath.length()-1 )
    {
        fixpath.append(sep);
    }
    return fixpath;
}

//
// fixup a path and insert it in the folder list
// Pop an item to the top of the list so that the list
// is in "Most recently used" order.
//
QString PhotoOpenDialog::fixAndInsertPath( QString path )
{
    // fix the nonsense with the separators
    QString fixpath = fixFolderPath( path );

    // move the folder to the top of the folder list combo box.
    // force a move to top by removing the item from the list first.
    int idx = ui->cbFolderPath->findText(fixpath);
    if ( idx >= 0 )
    {
        ui->cbFolderPath->removeItem( idx );
    }
    ui->cbFolderPath->insertItem(0, fixpath );
    ui->cbFolderPath->setCurrentIndex( 0 );

    return fixpath;
}

//
// Uparrow button removes the last folder from the current path
// and then opens that one.
// Of course it probably is already in the list, so it will be
// moved to the top of the list again.
//
void PhotoOpenDialog::on_btnUpArrow_clicked()
{
    QString folder = ui->cbFolderPath->itemText( 0 );

    QChar sep = QDir::separator();
    // even on windows this is always sep because we fix it this way
    // Qt sometimes gives us slashes on windows, but we replace those.
    int nIdx = folder.lastIndexOf( sep, folder.length()-2);

    if ( nIdx > 0 )
    {
        // if there is any of the path left, then pop the last part off
        // and activate it.
        QString folderup = folder.left(nIdx+1);

        // Double \\ might be left if we are upping from a //nodename/ path.
        // and we don't want to leave \\ as a path.
        if ( folderup != QString("\\\\") )
        {
            // the next routine will fix and insert the path in the folder list
            on_cbFolderPath_activated( folderup );
        }
    }
}


