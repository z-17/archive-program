#ifndef PHOTOOPENDIALOG_H
#define PHOTOOPENDIALOG_H

#include <QtWidgets/QDialog>
#include <QSettings>
#include <QtWidgets/QAbstractItemView>

class QTreeWidgetItem;

namespace Ui {
    class PhotoOpenDialog;
}

class PhotoOpenDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PhotoOpenDialog(QWidget *parent = 0);
    ~PhotoOpenDialog();

    // удаляет из листа уже измененные изображения
    bool isResizedImage( QString path );

    // принимает параметры из настройки
    void setTitle( QString title );

    void setSelectionMode( QAbstractItemView::SelectionMode mode);

    // Возвращает пути до изображений
    QStringList selectedFiles();

private:
    Ui::PhotoOpenDialog *ui;
    QSettings*          m_pSettings;
    int                 m_nIconSize;    // размер иконок

    bool                m_bProcessingFolder;
    bool                m_bStopProcessingFolder;    // передает значение true cancel'у для остановки


    void setStatus( QString sts );

    // Достает путь
    QString fixAndInsertPath( QString path );
    QString fixFolderPath( QString path );

    // Сохраняет список уже просмотренных
    void createSettings();
    void saveSettings();
    void restoreSettings();

    // Сохраняет и прикрывает все
    void closeEvent ( QCloseEvent * event );

private slots:
    void on_btnUpArrow_clicked();
    void on_twFolder_itemActivated(QTreeWidgetItem* item, int column);
    void on_cbFolderPath_activated(QString );
    void on_cbIconSize_activated(QString );
    void on_btnBrowseFolder_clicked();
    void on_btnOpen_clicked();
    void on_btnCancel_clicked();
};

#endif // PHOTOOPENDIALOG_H
