#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include <QPixmap>
#include <vector>
class QListWidgetItem;

struct Image {

public:
   virtual void addImage(QString)=0;        // добавляем изображение в класс
   virtual ~Image();
   virtual QPixmap returnOneImage()=0;         // возвращаем 1ое изображение (или изображение, или первое из массива)
   virtual int returnNumImages()=0;         // возвращаем кол-во изображений
   virtual  std::vector<QPixmap> returnListImages()=0;

   virtual QString returnOnePath()=0;
   virtual std::vector<QString> returnListPath()=0;
};

struct ImageOne : public Image {
    QPixmap images;
    QString path;

    QPixmap returnOneImage();
    std::vector<QPixmap> returnListImages();
    int returnNumImages();
    void addImage(QString);
    QString returnOnePath();
    std::vector<QString> returnListPath();
    ImageOne();

};

struct ImageList : public Image {
    std::vector<QPixmap> images;
    std::vector<QString> path;

    std::vector<QPixmap> returnListImages();
    QPixmap returnOneImage();
    int returnNumImages();
    void addImage(QString);
    QString returnOnePath();
    std::vector<QString> returnListPath();
    ImageList();
};

struct Factory
{
public:
    virtual Image * CreateImage() = 0;
};

struct FactoryCreateOne: public Factory
{
public:
    Image * CreateImage();
};

struct FactoryCreateList: public Factory
{
public:
    Image * CreateImage();
};
namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    // Изображения с которыми мы будем работать(и лист изображений)
    QPixmap m_onePixmap;
    QPixmap m_listPixmap;

    // Путь до изображения
    QString     m_onePhotoPath;

    bool        m_bPhotosResizing;
    bool        m_bStopResizing;
    Image * image;

    void resizeEvent ( QResizeEvent * event );

    // при изменении размеров окна, изменение и картинок
    void resizeImages();

    // работа с путями уже использованных картинок
    QStringList removeResizedImages( QStringList inList );
    bool        isResizedImage( QString path );

    // Снизу в приложении есть статус операций. Строка
    void setStatus( QString sts );

    // Изменение 1 пикчи
    bool resizeOnePhoto( QString path, int nSize );
    bool grayScaleOnePhoto( QString path );

    // Имена пикчам
    void setListPhotoName( QString path );
    void setOnePhotoName( QString path );

private slots:
    void on_actionReport_Library_Paths_triggered();
    void on_actionReport_Image_Formats_triggered();
    void on_tabWidget_currentChanged(int index);
    void on_btnCancel_clicked();
    void on_lwPhotoList_itemSelectionChanged();
    void on_cbShrinkPhotoPreview_stateChanged(int );
    void on_cbShrinkListPreview_stateChanged(int );
    void on_splitter_splitterMoved(int pos, int index);
    void on_lwPhotoList_itemClicked(QListWidgetItem* item);
    void on_btnResizePhotoList_clicked();
    void on_btnGrayScaleImageList_clicked();
    void on_btnGrayScaleImage_clicked();
    void on_btnChoosePhotoList_clicked();
    void on_btnResizePhoto_clicked();
    void on_btnChoosePhoto_clicked();
    void on_btnClose_clicked();
    void on_actionAbout_triggered();
    void on_actionQuit_triggered();
};

#endif // MAINWINDOW_H
