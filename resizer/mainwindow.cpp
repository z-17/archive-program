#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutdialog.h"
#include "photoopendialog.h"

#include <QFileDialog>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QApplication>
#include <QImageReader>
#include <QDebug>



template <class T> const T& max ( const T& a, const T& b ) {
  return (b<a)?a:b;     // обычное сравнение для выявления максимума
}
template <class T> const T& min ( const T& a, const T& b ) {
  return (b>a)?a:b;     // минимум( при работе с разными платформами необходимо описать так тупо
}

#if 0
int max( int one, int two )
{
    if ( one > two )    return one;
    else                return two;

}

int min( int one, int two )
{
    if ( one < two )    return one;
    else                return two;
}
#endif

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_bPhotosResizing = false;
    m_bStopResizing = false;


    if ( QDir::separator() == QChar('\\') )
    {
        QString appPath = qApp->applicationDirPath() +QString("/plugins");
        qApp->addLibraryPath( appPath );
        qDebug( )<< tr("Windows");
    }

    qDebug() << qApp->libraryPaths();
    qDebug() << QImageReader::supportedImageFormats();


#if 0
    // Установка пути до плагинов и библиотек
    QStringList libPaths;
    libPaths = qApp->libraryPaths();
    for ( int i=0; i< libPaths.count(); i++ )
    {
        QString str = libPaths[i];
        ui->lwPhotoList->insertItem( i, str );

    }
#endif

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionQuit_triggered()
{
    qApp->exit();
}

void MainWindow::on_actionAbout_triggered()
{
    if ( m_bPhotosResizing )
    {   return;
    }
    AboutDialog dialog(this);
    dialog.exec();
}

void MainWindow::on_btnClose_clicked()
{
    qApp->exit();
}


void MainWindow::resizeEvent ( QResizeEvent * event )
{
    setStatus("");

    resizeImages();
}
void MainWindow::setStatus( QString sts )
{
    ui->statusBar->showMessage( sts );
}

Image::~Image() {
}

ImageOne::ImageOne()
{
}

void ImageOne::addImage(QString _path)
{
    path = _path;
    images.load(path);
}
QPixmap ImageOne::returnOneImage()
{
    return images;
}
int ImageOne::returnNumImages()
{
    return 1;
}

ImageList::ImageList()
{
}
void ImageList::addImage(QString _path)
{
    QPixmap newImage;
    newImage.load(_path);
    images.push_back( newImage);
    path.push_back(_path);
}
QPixmap ImageList::returnOneImage()
{
    return images[0];
}
int ImageList::returnNumImages()
{
    return images.size();
}

std::vector<QPixmap> ImageOne::returnListImages()
{
    std::vector<QPixmap> imagesList;
    imagesList.push_back(images);
    return imagesList;
}
std::vector<QPixmap> ImageList::returnListImages()
{
    return images;
}
QString ImageOne::returnOnePath()
{
    return path;
}
QString ImageList::returnOnePath()
{
    return path[0];
}
std::vector<QString> ImageOne::returnListPath()
{
    std::vector<QString> pathList;
    pathList.push_back(path);
    return pathList;
}
std::vector<QString> ImageList::returnListPath()
{
    return path;
}


// фабрика
Image * FactoryCreateOne::CreateImage()
{
    Image * one = new ImageOne();
    return one;
}
Image * FactoryCreateList::CreateImage()
{
    Image * one = new ImageList();
    return one;
}
//--------------------------------------------------------------------------------------------------

class Algoritm
{
public:
    Algoritm(){}
    ~Algoritm(){}

    virtual void use(Image * image, int nSize) = 0;
};

class grayScale: public Algoritm
{
public:
        grayScale(){}
        ~grayScale(){}
       void use(Image * image, int nSize)
        {
            QPixmap currImages = image->returnOneImage();
            //MainWindow.setStatus("");
            if( ! currImages.isNull())
            {
                QImage img = currImages.toImage();
                img = img.convertToFormat(QImage::Format_Indexed8, Qt::MonoOnly);

                QPixmap newPix = QPixmap::fromImage( img );
                if ( newPix.isNull() )
                {
                    //MainWindow.setStatus(tr("Error compressing the image."));
                    return;
                }
                // новое имя для изображения
                QString newPath = image->returnOnePath();
                int idx = newPath.lastIndexOf( QChar('.'));
                newPath.insert(idx, QString("__grey"));

                // Если уже есть такой файл - спрашиваем
                if ( QFile(newPath).exists() )
                {
                    //setStatus(tr("Grayscaled image exists."));
                    QMessageBox msgBox;
                    msgBox.setText( MainWindow::tr("This Image Has Been grayscaled.") );
                    msgBox.setInformativeText(MainWindow::tr("Compress and replace it  ?") );
                    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel );
                    msgBox.setDefaultButton(QMessageBox::Cancel);
                    msgBox.setIcon(QMessageBox::Warning);
                    int ret = msgBox.exec();
                    if ( ret != QMessageBox::Save )
                    {
                        QMessageBox errorBox;
                        errorBox.setText(MainWindow::tr("Image not grayscaled."));
                        errorBox.setStandardButtons(QMessageBox::Ok);
                        msgBox.setIcon(QMessageBox::Warning);
                        return;
                    }
                }
                // ошибка
                if ( !newPix.save( newPath ) )
                {
                   // MainWindow.setStatus(tr("Error saving image. Is it readonly, or on a CD-ROM?"));
                }
                else
                {
                    //MainWindow.setStatus(tr("Image compressed!"));
                }
            }
            else
            {
        //        MainWindow.setStatus(tr("Open an image first."));
            }

        }
};

class grayScaleList: public Algoritm
{
public:
        grayScaleList(){}
        ~grayScaleList(){}
       void use(Image * image, int nSize)
        {
           std::vector<QPixmap> list = image->returnListImages();
           std::vector<QString> listPath = image->returnListPath();
           int nItemsGrayscaled = 0;
           bool bOk = false;
           for (int i =0; i < list.size(); i++)
           {
                QImage img = list[i].toImage();
                img = img.convertToFormat(QImage::Format_Indexed8, Qt::MonoOnly);

                QPixmap newPix = QPixmap::fromImage( img );
                if ( newPix.isNull() )
                {
                    bOk = false;
                       break;
                }
                // новое имя для изображения
                QString newPath = listPath[i];
                int idx = newPath.lastIndexOf( QChar('.'));
                newPath.insert(idx, QString("__grey"));

                // Если уже есть такой файл - спрашиваем
                if ( QFile(newPath).exists() )
                {
                    QMessageBox msgBox;
                    msgBox.setText( MainWindow::tr("This Image Has Been grayscaled.") );
                    msgBox.setInformativeText(MainWindow::tr("Compress and replace it?") );
                    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel );
                    msgBox.setDefaultButton(QMessageBox::Cancel);
                    msgBox.setIcon(QMessageBox::Warning);
                       int ret = msgBox.exec();
                       if ( ret != QMessageBox::Save )
                       {
                           bOk = false;
                           break;
                       }
                    }
                    //ошибка
                    if ( !newPix.save( newPath ) )
                    {
                    }
                    else
                    {
                       nItemsGrayscaled++;
                    }
               }
        }
};

class resizePix: public Algoritm
{
public:
    resizePix(){}
    ~resizePix(){}
    void use(Image * image,int nSize)
    {
        //setStatus("");

        QPixmap currImages = image->returnOneImage();
        // проверка на открытую пикчу
        if ( ! currImages.isNull() )
        {
            //int nSize = ui->cbSize->currentText().toInt();

            // проверка на размер(только сжимаем, не увеличиваем)
            int nLongSide = max( currImages.height(), currImages.width() );
            if ( nLongSide < nSize )
            {
                //setStatus(tr("Image is %1, which is smaller than the size requested. Not resized.").arg(nLongSide));
                return;
            }

            // Выполняем сам ресайз

            int newH = min( nSize, currImages.height() );
            int newW = min( nSize, currImages.width() );
            QPixmap newPix = currImages.scaled ( newW, newH, Qt::KeepAspectRatio, Qt::SmoothTransformation );

            // Ошибочки
            if ( newPix.isNull() )
            {
                //setStatus(tr("Error resizing the image."));
                return;
            }
            // Опять переименовываем(см пред функцию)
            QString newPath = image->returnOnePath();
            int idx = newPath.lastIndexOf( QChar('.'));
            newPath.insert(idx, QString("x%1").arg(nSize));

            if ( QFile(newPath).exists() )
            {
                QMessageBox msgBox;
                msgBox.setText( MainWindow::tr("This Image Has Been Resized to the selected size.") );
                msgBox.setInformativeText(MainWindow::tr("Resize and replace it?") );
                msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel );
                msgBox.setDefaultButton(QMessageBox::Cancel);
                msgBox.setIcon(QMessageBox::Warning);
                int ret = msgBox.exec();
                if ( ret != QMessageBox::Save )
                {
                    //setStatus(tr("Image not resized."));
                    return;
                }
            }
            if ( !newPix.save( newPath ) )
            {
                //setStatus(tr("Error saving image. Is it readonly, or on a CD-ROM?"));
            }
            else
            {
                // Докладываем в месседж строку что сделали
                //setStatus(tr("Image resized to: (%1,%2) %3")
                //          .arg(newPix.width()).arg(newPix.height()).arg(newPath) );
            }
        }
        else
        {
        //    setStatus(tr("Open an image first."));
        }


    }
};

class resizeListPix: public Algoritm
{
public:
   resizeListPix(){}
    ~resizeListPix(){}
    void use(Image * image, int nSize)
    {
        bool bOk;
        int  nResize=0;
        std::vector<QPixmap> list = image->returnListImages();
        std::vector<QString> listPath = image->returnListPath();

        for (int i =0; i < list.size(); i++)
        {
            QPixmap currImages = list[i];
            int newH = min( nSize, currImages.height() );
            int newW = min( nSize, currImages.width() );
            QPixmap newPix = currImages.scaled ( newW, newH, Qt::KeepAspectRatio, Qt::SmoothTransformation );
            if ( newPix.isNull() )
            {
                break;
            }
            // Опять переименовываем(см пред функцию)
            QString newPath = listPath[i];
            int idx = newPath.lastIndexOf( QChar('.'));
            newPath.insert(idx, QString("x%1").arg(nSize));
            if ( QFile(newPath).exists() )
            {
                QMessageBox msgBox;
                msgBox.setText( MainWindow::tr("This Image Has Been Resized to the selected size.") );
                msgBox.setInformativeText(MainWindow::tr("Resize and replace it?") );
                msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel );
                msgBox.setDefaultButton(QMessageBox::Cancel);
                msgBox.setIcon(QMessageBox::Warning);
                int ret = msgBox.exec();
                if ( ret != QMessageBox::Save )
                {
                   return;
                }
            }
            if ( !newPix.save( newPath ) )
            {
                bOk = false;
            }
            else
            {
             nResize++;
            }

        }

    }
};

class Context
{
protected:
       Algoritm* operation;

public:
    Context(void){}
    ~Context(void){}

    virtual void UseAlgoritm(Image * image, int nSize) = 0;
    virtual void SetAlgoritm(int a) = 0;
};
class UseCompression: public Context
{
    public:
    UseCompression(){}
    ~UseCompression(){}

    void UseAlgoritm(Image * image, int nSize)
    {
        operation->use(image, nSize);
    }

    void SetAlgoritm(int a)
    {
        if (a == 5)
        {
            resizePix * test = new resizePix;
            operation = test;
        }
        else if (a == 4)
        {
            resizeListPix * test = new resizeListPix;
            operation = test;
        }
        else if (a == 3)
        {
            grayScaleList * test = new grayScaleList;
            operation = test;
        }
        else
        {
            grayScale * test = new grayScale;
            operation = test;
        }
     /*   switch (a)
        {
            case 5:
                resizePix * test = new resizePix;
                operation = test;
                break;
            case 4:
                resizeListPix * test = new resizeListPix;
                operation = test;
                break;
            default:
                grayScale * test = new grayScale;
                operation = test;
                break;
       }
*/

    }
};




void MainWindow::setListPhotoName( QString path )
{
    QFileInfo fi;
    fi.setFile(path);
    ui->lbPhotoListName->setText( fi.fileName() );
}

void MainWindow::setOnePhotoName( QString path )
{
    QFileInfo fi;
    fi.setFile(path);
    ui->lbOnePhotoName->setText( fi.fileName() );
}

void MainWindow::resizeImages()
{
    if ( !m_onePixmap.isNull() )
    {
        QPixmap pixFit;

        if ( ui->cbShrinkPhotoPreview->isChecked() )
        {
            //если сжимается - жмем(окно предпросмотра изображений)
            pixFit = m_onePixmap.scaled(ui->gvPhotoPreview->width()-2,
                                            ui->gvPhotoPreview->height()-2, Qt::KeepAspectRatio );
        }
        else
        {
            pixFit = m_onePixmap;
        }

        ui->gvPhotoPreview->scene()->clear();
        ui->gvPhotoPreview->scene()->addPixmap( pixFit );

        // подгон поля скроллинга под конкретное изображение
        ui->gvPhotoPreview->setSceneRect( ui->gvPhotoPreview->scene()->itemsBoundingRect() );
    }

    if ( !m_listPixmap.isNull() )
    {
        QPixmap pixFit;

        if ( ui->cbShrinkListPreview->isChecked() )
        {
            // Сжимается -> жмем
            pixFit = m_listPixmap.scaled(ui->gvPhotoListPreview->width()-2,
                                        ui->gvPhotoListPreview->height()-2, Qt::KeepAspectRatio );
        }
        else
        {
            pixFit = m_listPixmap;
        }

        ui->gvPhotoListPreview->scene()->clear();
        ui->gvPhotoListPreview->scene()->addPixmap( pixFit );

        // Подгон под размер изображения 2
        ui->gvPhotoListPreview->setSceneRect( ui->gvPhotoListPreview->scene()->itemsBoundingRect() );
    }
}

// разбираемся с уже сжатыми изображениями
QStringList MainWindow::removeResizedImages( QStringList inList )
{
    int cnt = inList.count();
    QStringList outList;

    bool    bAnyResized = false;

    for ( int i=0; i < cnt; i++ )
    {
        if ( isResizedImage( inList[i]) )
        {
            bAnyResized = true;
        }
        else
        {
            outList.append( inList[i] );
        }
    }

    if ( bAnyResized )
    {
#if 0
        QMessageBox msgBox;
        msgBox.setText( tr("Some Images Cannot Be Resized.") );
        msgBox.setInformativeText(tr("Some resized images have been removed from the list.") );
        msgBox.setStandardButtons(QMessageBox::Ok );
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
#endif
        setStatus( tr("Some resized images have been removed from the list."));
    }
    return outList;
}

// Добавляем в имя сжатого изображения параметры сжатия
bool MainWindow::isResizedImage( QString path )
{
    QString foo;

    // filenamexnnnn.jpg
    QRegExp re(QString("x{1,1}[0-9]{1,4}\\.{1,1}"), Qt::CaseInsensitive );

    return path.contains( re );
}

// выбор 1 изображения для сжатия
void MainWindow::on_btnChoosePhoto_clicked()
{
    if ( m_bPhotosResizing )
    {   return;
    }
    setStatus("");

    PhotoOpenDialog dialog(this);

    dialog.setTitle(tr("Choose Image"));
    dialog.setSelectionMode( QAbstractItemView::SingleSelection );

    QStringList fileNames;
    if (dialog.exec())
    {
        m_onePhotoPath = "";

        fileNames = dialog.selectedFiles();
        QString path = fileNames[0];

        if ( this->isResizedImage( path ) )
        {
#if 0
            QMessageBox msgBox;
            msgBox.setText( tr("This Image Has Been Resized.") );
            msgBox.setInformativeText(tr("Select an image that has not been resized.") );
            msgBox.setStandardButtons(QMessageBox::Ok );
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();
#endif
            setStatus(tr("Select an image that has not been resized."));
            path = "";
            ui->lbOnePhotoName->setText("");
        }
        // Загружаем из пути указанного

         QGraphicsScene  *pScene = new QGraphicsScene(); // empty scene

         // Пустая "сцена" для просмотра
         ui->gvPhotoPreview->setScene( pScene );
         ui->gvPhotoPreview->setDragMode( QGraphicsView::ScrollHandDrag );

         FactoryCreateOne creator;
         image = creator.CreateImage();
         image->addImage(path);

         // Если пикча норм - загружаем в сцену
         if ( m_onePixmap.load( path ) )
         {
             m_onePhotoPath = path;
             setOnePhotoName( path );

             setStatus(tr("Image read. Size (%1,%2)").arg(m_onePixmap.width()).arg(m_onePixmap.height()));

             // Изменяем размеры изоюражения в зависимости от настроек
             resizeImages();
         }
    }

}

void MainWindow::on_btnGrayScaleImage_clicked()
{
    UseCompression compressAlgoritms;
    compressAlgoritms.SetAlgoritm(2);
    compressAlgoritms.UseAlgoritm(image, NULL);
}



// ресайз 1 пикчи
void MainWindow::on_btnResizePhoto_clicked()
{
    int nSize = ui->cbSize->currentText().toInt();
    UseCompression compressAlgoritms;
    compressAlgoritms.SetAlgoritm(5);
    compressAlgoritms.UseAlgoritm(image, nSize);

}

// Выбор нескольких фото
void MainWindow::on_btnChoosePhotoList_clicked()
{
    if ( m_bPhotosResizing )
    {   return;
    }
    setStatus("");

    PhotoOpenDialog dialog(this);

    dialog.setTitle(tr("Choose Several Photos"));
    dialog.setSelectionMode( QAbstractItemView::ExtendedSelection );

    QStringList fileNames;
    if (dialog.exec())
    {
        fileNames = dialog.selectedFiles();
        if ( fileNames.count())
        {
            // Удаление уже сжатых
            fileNames = removeResizedImages( fileNames );
            qDebug("on_btnChoosePhotoList_clicked - about to clear");

            int nitems = ui->lwPhotoList->count();
            qDebug("on_btnChoosePhotoList_clicked - Items %d", nitems);
            // убираем все кроме 1
            for ( int i=0; i<nitems-1; i++ )
            {
                qDebug("on_btnChoosePhotoList_clicked - take item %d", i);
                QListWidgetItem* item = ui->lwPhotoList->takeItem( 0 );
                qDebug("on_btnChoosePhotoList_clicked - delete item %d", i);
                delete item;
            }
            qDebug("on_btnChoosePhotoList_clicked - removed all items");

            // в листе должен быть хотя бы 1 элемент всегда
           /* if ( nitems == 0 )
            {
                ui->lwPhotoList->insertItem(0, QString("") );
            }

            qDebug("on_btnChoosePhotoList_clicked - Clear, about to insert.");
*/
            FactoryCreateList creator;
            image = creator.CreateImage();
#if 1 // Простой способ
           // QListWidgetItem* pitem = ui->lwPhotoList->item( 0 );
           // pitem->setText( fileNames[0] );
           // fileNames.removeFirst();
            // Если еще остались элементы - суем их
           /* if ( fileNames.count() )
            {
                ui->lwPhotoList->insertItems(0, fileNames );
            }*/
             for (int i = 0; i < fileNames.count(); i++)
             {
         //        ui->lwPhotoList->insertItems(0, fileNames );
                 image->addImage(fileNames[i]);
             }

            if ( fileNames.count() )
            {
                ui->lwPhotoList->insertItems(0, fileNames );
            }
#endif // Еще один
            qDebug("on_btnChoosePhotoList_clicked - items inserted");

            QGraphicsScene  *pScene = new QGraphicsScene(); // empty scene

            // add empty scene to the view.
            ui->gvPhotoListPreview->setScene( pScene );
            ui->gvPhotoListPreview->setDragMode( QGraphicsView::ScrollHandDrag );

            // sort the file list now
            ui->lwPhotoList->sortItems();

            // if it's a valid image, then load the first image into the scene.
            QString path = ui->lwPhotoList->item(0)->text();
            ui->lwPhotoList->setCurrentRow(0);
            // set the path above the image.
            setListPhotoName( path );
            if ( m_listPixmap.load( path ) )
            {
                // Display the image if we got one
                resizeImages();
            }
        }
    }
}

bool MainWindow::grayScaleOnePhoto(QString path)
{
    if( m_listPixmap.load( path ) )
    {
        setListPhotoName( path );
        resizeImages();
    }
    else
    {
        setStatus(tr("Error reading image: %1").arg(path));
        return false;
    }
    QImage img = m_listPixmap.toImage();
    img = img.convertToFormat(QImage::Format_Indexed8, Qt::MonoOnly);

    QPixmap newPix = QPixmap::fromImage( img );
        if ( newPix.isNull() )
        {
            setStatus(tr("Error compressing the image."));
            return false;
        }
        // новое имя для изображения
        QString newPath = path;
        int idx = newPath.lastIndexOf( QChar('.'));
        newPath.insert(idx, QString("__grey"));

        // Если уже есть такой файл - спрашиваем
        if ( QFile(newPath).exists() )
        {
            //setStatus(tr("Grayscaled image exists."));
            QMessageBox msgBox;
            msgBox.setText( tr("This Image Has Been grayscaled.") );
            msgBox.setInformativeText(tr("Compress and replace it?") );
            msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel );
            msgBox.setDefaultButton(QMessageBox::Cancel);
            msgBox.setIcon(QMessageBox::Warning);
            int ret = msgBox.exec();
            if ( ret != QMessageBox::Save )
            {
                setStatus(tr("Image not grayscaled."));
                return false;
            }
        }
        // ошибка
        if ( !newPix.save( newPath ) )
        {
            setStatus(tr("Error saving image. Is it readonly, or on a CD-ROM?"));
        }
        else
        {
            setStatus(tr("Image compressed!"));
        }
}
void MainWindow::on_btnGrayScaleImageList_clicked()
{
    UseCompression compressAlgoritms;
    compressAlgoritms.SetAlgoritm(3);
    compressAlgoritms.UseAlgoritm(image, NULL);
}


// Сжимаем 1 изображениеи передаем true если продолжаем потом
bool MainWindow::resizeOnePhoto( QString path, int nSize )
{

    // загружаем пикчу
     if ( m_listPixmap.load( path ) )
     {
         setListPhotoName( path );
         // Ресайзим под размер окна
         resizeImages();
     }
     else
     {
         // Ошибки
         setStatus(tr("Error reading image: %1").arg(path));
         return false;
     }
    // Проверка на размер изображений
    int nLongSide = max( m_listPixmap.height(), m_listPixmap.width() );
    if ( nLongSide < nSize )
    {
        setStatus(tr("Image is %1, which is smaller than the size requested. Not resized.").arg(nLongSide));
        return true;
    }

    // Сам ресайз
    int newH = min( nSize, m_listPixmap.height() );
    int newW = min( nSize, m_listPixmap.width() );
    QPixmap newPix = m_listPixmap.scaled ( newW, newH, Qt::KeepAspectRatio, Qt::SmoothTransformation );

    // ошибки
    if ( newPix.isNull() )
    {
        setStatus(tr("Error resizing the image: %1").arg(path));
        return false;
    }
    // опять новое имя
    QString newPath = path;
    int idx = newPath.lastIndexOf( QChar('.'));
    newPath.insert(idx, QString("x%1").arg(nSize));

    if ( QFile(newPath).exists() )
    {
        QMessageBox msgBox;
        msgBox.setText( tr("The Image Has Been Resized to the selected size.") );
        msgBox.setInformativeText(tr("Resize and replace?\n%1").arg(newPath) );
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel );
        msgBox.setDefaultButton(QMessageBox::Cancel);
        msgBox.setIcon(QMessageBox::Warning);
        int ret = msgBox.exec();
        if ( ret != QMessageBox::Save )
        {
            setStatus(tr("Image not resized."));
            return false;
        }
    }
    if ( !newPix.save( newPath ) )
    {
        setStatus(tr("Error saving image. Is it readonly, or on a CD-ROM?"));
        return false;
    }
    else
    {
        setStatus(tr("Image resized to: (%1,%2) %3")
                  .arg(newPix.width()).arg(newPix.height()).arg(newPath) );
    }
    return true;
}


// Сжатие нескольких изображений
void MainWindow::on_btnResizePhotoList_clicked()
{
    int nSize = ui->cbSizeList->currentText().toInt();
    UseCompression compressAlgoritms;
    compressAlgoritms.SetAlgoritm(4);
    compressAlgoritms.UseAlgoritm(image, nSize);
    setStatus(tr("All images resized successfully."));
}

// Фото для превью
void MainWindow::on_lwPhotoList_itemClicked(QListWidgetItem* item)
{
    if ( m_bPhotosResizing )
    {   return;
    }
    setStatus("");

    QString path = item->text();
    setListPhotoName( path );

    // Загружаем картинки(+помещаем в сцену)
     if ( m_listPixmap.load( path ) )
     {
         setStatus(tr("Previewing: %1").arg(path));
         // Изменяем размер в зависимости от параметров
         resizeImages();
     }
}

// Еслим меняется выбор - меняем превью
void MainWindow::on_lwPhotoList_itemSelectionChanged()
{
    if ( m_bPhotosResizing )
    {   return;
    }
    QList<QListWidgetItem*> items = ui->lwPhotoList->selectedItems();

    on_lwPhotoList_itemClicked( items[0] );
}

void MainWindow::on_splitter_splitterMoved(int pos, int index)
{
    setStatus("");

   resizeImages();
}

void MainWindow::on_cbShrinkListPreview_stateChanged(int )
{
    setStatus("");

    resizeImages();
}

void MainWindow::on_cbShrinkPhotoPreview_stateChanged(int )
{
    setStatus("");

    resizeImages();
}

// Кнопка Cancel при ресайзе нескольких элементов
void MainWindow::on_btnCancel_clicked()
{
    if ( m_bPhotosResizing )
    {
        m_bStopResizing = true;
    }
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    resizeImages();
}
//Эта функция выводит форматы поддерживаемые
void MainWindow::on_actionReport_Image_Formats_triggered()
{
    QString sts;
    QString supportedImageFormats;
    for (int formatIndex = 0; formatIndex < QImageReader::supportedImageFormats().count(); formatIndex++) {
        supportedImageFormats += QLatin1String(" ") + QImageReader::supportedImageFormats()[formatIndex];
    }
    sts = QString("Supported Image Formats: %1").arg(supportedImageFormats);
    setStatus( sts );
}

void MainWindow::on_actionReport_Library_Paths_triggered()
{
    QString sts;

    for (int i=0; i < qApp->libraryPaths().count(); i++ )
    {
        sts += QString(";") + qApp->libraryPaths()[i];
    }
    setStatus( sts );
}
