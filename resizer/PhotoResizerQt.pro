#-------------------------------------------------
#
# Project created by QtCreator 2010-12-29T09:38:05
#
#-------------------------------------------------

QT       += core gui\
            widgets

TARGET = PhotoResizerQt
TEMPLATE = app
win32:RC_FILE = winicon.rc

SOURCES += main.cpp\
        mainwindow.cpp \
    aboutdialog.cpp \
    photoopendialog.cpp

HEADERS  += mainwindow.h \
    aboutdialog.h \
    photoopendialog.h

FORMS    += mainwindow.ui \
    aboutdialog.ui \
    photoopendialog.ui

OTHER_FILES += \
    winicon.rc

RESOURCES += \
    resources.qrc
