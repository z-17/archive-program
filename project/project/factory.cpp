#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Image
{
private:
	int size_vertical;
	int size_horizontal;
public:
	virtual void test() = 0;
	virtual ~Image() {}

	int get_size_v()
	{
		return this->size_vertical;
	}	
	int get_size_h()
	{
		return this->size_horizontal;
	}
};

class CreateJpg: public Image
{
public:
	void test()
	{
		cout << "CreateJpg" << endl;
	}
};

class CreatePng: public Image
{
public:
	void test()
	{
		cout << "CreatePng" << endl;
	}
};

struct Factory
{
public: 
	virtual Image* CreateImage() = 0;
};

class FactoryCreateJpg: public Factory
{
public:
	Image* CreateImage() 
	{
		return new CreateJpg();
	}
};
 
class FactoryCreatePng: public Factory
{
public: 
	Image* CreateImage() 
	{
		return new CreatePng();
	}
};