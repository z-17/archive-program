#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include "wrapper.cpp"

using namespace std;


/*

������ � wrapper �����:
1) �������� ����� addImages:
	��������� fileguide, �� ��� ���������� ���-�� (������� ���� �� ��������)
	���������  addJpgImage ���  addPngImage ��� �����, ��� ������ fileguide
2) �������� ����� go
	����������� ���������, �������� ��������
3) ��������� ����� saveImages
	��������� ��������
*/

int main()
{
	wrapper letsPlay;
	letsPlay.addJpgImage();
	letsPlay.addPngImage();
	letsPlay.test();
	letsPlay.go();

	system("pause");
	return 0;
}
